# README #

It's a software for basic communication between your fitness band bracelet and your computer.
It draws nice graphs if you have a mysql database and apache webserver with php functionality.

### Features ###
* Fitness band synchronization by bluetooth
* Data download and upload to and from your fitness band device
* Date and time settings 
* User weight, height, gender and age settings. 
* Csv file export and Mysql database supported. 
* Draws graphs on a webpage with your fitness activities displayed

Bash scripts. Bluetooth Low Energy. I5 and I6 fitness bands supported.

* Version 0.02.

Planned to add in version 0.03: Support for multiple devices and a wireless scale 

### How do I get set up? ###

You need:

* Fitness band.  Like the one which can be found in various places, for example this:
[Bracelet from China](http://www.aliexpress.com/wholesale?SearchText=fitness+bracelet+i5&isRtl=yes)

* Bluetooth 4.0 Low Energy capable usb stick or laptop with bluetooth 4.0

* Linux, Unix machine. Tested with Ubuntu.

* If you want to save daily data and results in a database: Mysql server login pass 

* If you want to see graphs: Apache webserver with php functionality 

### Summary of set up and Configuration ###

Download the sources:

```
#!bash

git clone https://bitbucket.org/alex_mail/fitbandsync
```

You might need to run:

```
#!bash

sudo apt-get install git

```

Edit the variables in the bracelet_i5.sh file: 
To obtain these run:

```
#!bash

hciconfig -a 
hcitool lescan 
```

(as root)

press button twice on your bracelet, or wait for it to become visible

Optionally create database and tables using the file tables_bracelet_i5.sql and copy the files from PHP directory to your web server.

Edit your Age / Weight/ Height in bracelet_i5.sh if you want them to be initially set up.

You can also use the provided web pages to upload them to your bracelet.
 
Optionally: Open the "Alarms" web page to set alarms on your bracelet.

 Run: 

```
#!bash

bracelet_i5.sh

```
or, to run continuously every so many minutes run:

```
#!bash

watchrun.sh 

```

### Contribution guidelines       Perl and Php programmers needed ! ###

php pages to display daily data and or graphs would be welcome!
Any comments and remarks, especially how to improve the software and make a server running in background update mode are very welcome. Maybe needed to rewrite in another language, for example Perl. If you have needed skills, welcome to contribute!


---
Fitbit Flex Charge HR Surge as well as Jawbone and Pebble etc. are trademarks of their respective owners, not related to this software. (also: health records pedometer step counter, etc)
Ubuntu Linux and Unix are operating systems on and for which this software was developed.
There is no warranty that this software is fit for any purpose, use at your own risk.