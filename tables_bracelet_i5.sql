
DROP TABLE IF EXISTS daily;
CREATE TABLE IF NOT EXISTS `daily` (
  `daily_id` int(11) NOT NULL AUTO_INCREMENT,
  `bcc` integer,
  `date` varchar(32),
  `steps` integer,
  `distance` integer,
  `cal` integer,
  PRIMARY KEY (`daily_id`)
) ;

DROP TABLE IF EXISTS sport;
CREATE TABLE IF NOT EXISTS `sport` (
  `sport_id` int(11) NOT NULL AUTO_INCREMENT,
  `bcc` integer,
  `date` varchar(32),
  `time` varchar(32),
  `steps` integer,
  `distance` integer,
  `cal` integer,
  `flag` integer,
  PRIMARY KEY (`sport_id`)
) ;

ALTER TABLE sport add unique INDEX datetime (date,time) ;

delete from daily where date='0-01-01' or date='255-256-256' ;
delete from sport where date='0-01-01' or date='255-256-256' ;

DROP TABLE IF EXISTS hour;
CREATE TABLE IF NOT EXISTS `hour` (
  `hh` varchar(32)
) ;

insert into hour(hh) values ('00');
insert into hour(hh) values ('01');
insert into hour(hh) values ('02');
insert into hour(hh) values ('03');
insert into hour(hh) values ('04');
insert into hour(hh) values ('05');
insert into hour(hh) values ('06');
insert into hour(hh) values ('07');
insert into hour(hh) values ('08');
insert into hour(hh) values ('09');
insert into hour(hh) values ('10');
insert into hour(hh) values ('11');
insert into hour(hh) values ('12');
insert into hour(hh) values ('13');
insert into hour(hh) values ('14');
insert into hour(hh) values ('15');
insert into hour(hh) values ('16');
insert into hour(hh) values ('17');
insert into hour(hh) values ('18');
insert into hour(hh) values ('19');
insert into hour(hh) values ('20');
insert into hour(hh) values ('21');
insert into hour(hh) values ('22');
insert into hour(hh) values ('23');

DROP TABLE IF EXISTS minute;
CREATE TABLE IF NOT EXISTS `minute` (
  `mm` varchar(32)
) ;

insert into minute(mm) values ('00');
insert into minute(mm) values ('05');
insert into minute(mm) values ('10');
insert into minute(mm) values ('15');
insert into minute(mm) values ('20');
insert into minute(mm) values ('25');
insert into minute(mm) values ('30');
insert into minute(mm) values ('35');
insert into minute(mm) values ('40');
insert into minute(mm) values ('45');
insert into minute(mm) values ('50');
insert into minute(mm) values ('55');


DROP TABLE IF EXISTS hhmm;
CREATE TABLE IF NOT EXISTS `hhmm` (
  `hhmm_id` int(11) NOT NULL AUTO_INCREMENT,
  `hhmm` varchar(32),
   PRIMARY KEY (`hhmm_id`)
) ;

insert into hhmm(hhmm) SELECT concat(h.hh,':', m.mm) 
     FROM hour h 
     JOIN minute m 
     GROUP BY h.hh, m.mm
     order by hh asc, mm asc ;

ALTER TABLE hhmm add unique INDEX hhmm (hhmm);

DROP TABLE IF EXISTS hour;
DROP TABLE IF EXISTS minute;

drop view if exists vsport;
create view vsport as select bcc, date, time, steps, cal, flag from sport
union select -1, null, hhmm, 0, 0, 0 from hhmm ;


DROP TABLE IF EXISTS battery;
CREATE TABLE IF NOT EXISTS `battery` (
  `battery_id` int(11) NOT NULL AUTO_INCREMENT,
  `date` varchar(32),
  `time` varchar(32),
  `battery` integer,
  `flag` integer,
  PRIMARY KEY (`battery_id`)
) ;

DROP TABLE IF EXISTS activity; 
CREATE TABLE IF NOT EXISTS  `activity` (
  `date` varchar(32) DEFAULT NULL,
  `sleep` decimal(6,2) DEFAULT NULL,
  `act1` decimal(8,0) DEFAULT NULL,
  `act2` decimal(8,0) DEFAULT NULL,
  `act3` decimal(8,0) DEFAULT NULL,
  `act4` decimal(8,0) DEFAULT NULL
) ;
ALTER TABLE activity add unique INDEX date (date);

DROP TABLE IF EXISTS alarm;
CREATE TABLE `alarm` (
  `alarm_id` int NOT NULL AUTO_INCREMENT,
  `bdev` char(18) DEFAULT NULL,
  `n` int NOT NULL DEFAULT '0',
  `wdays` int NOT NULL DEFAULT '0',
  `hh` int NOT NULL DEFAULT '0',
  `mm` int NOT NULL DEFAULT '0',
  `sync` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`alarm_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='storage for alarms for wake up and other purposes';

ALTER TABLE alarm add INDEX bdev (bdev);
ALTER TABLE alarm add INDEX n (n);
ALTER TABLE alarm add UNIQUE INDEX alarm (bdev,n);

/*update sport set bcc=10, steps=10, cal=10, flag=0 where date = '15-01-30' and time>'07:35' and time < '11:23' */

DROP TABLE IF EXISTS user;
CREATE TABLE `user` (
  `user_id` int NOT NULL AUTO_INCREMENT,
  `bdev` char(18) DEFAULT NULL,
  `height` int NOT NULL DEFAULT '170',
  `weight` int NOT NULL DEFAULT '75',
  `gender` int NOT NULL DEFAULT '1',
  `age` int NOT NULL DEFAULT '35',
  `stepsgoal` int NOT NULL DEFAULT '10000',
  `sync` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='user settings and goals';
ALTER TABLE user add unique INDEX bdev (bdev);
