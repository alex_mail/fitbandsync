#!/bin/bash

#device identificators
HCI=hci1
HCIBLOCKED=hci0
BDEV=A1:10:34:00:25:FE

# DB username and password
#leave empty DBNAME if you don't want to run the database functions
DBHOST=localhost
DBUSER=fituser
DBPASS=***
DBNAME=dbname

#User data for sending to the bracelet in the function setUserData
HEIGHT=170
WEIGHT=65
#gender: male 0, female 1
MF=1
AGE=24
STEPSGOAL=5000

###################################### INITIALIZATION BT CONNECTION ###################################
Init ()
{
stop bluetooth
hciconfig $HCI reset
hciconfig $HCIBLOCKED down; hciconfig $HCI up
#hciconfig -a
#sleep 1

echo -e "\n\npress button twice on your bracelet, or wait for it to initiate connection\n"

hcitool -i $HCI con
hcitool -i $HCI lewlclr
hcitool -i $HCI lewladd $BDEV
hcitool -i $HCI lecc $BDEV
#hcitool -i $HCI con
clear
echo -e "\nResults: ---------------------------------------"

}

#END ###################################### INITIALIZATION BT CONNECTION ###################################


#gatttool -i $HCI -t random -b $BDEV --primary
#gatttool -i $HCI -t random -b $BDEV --characteristics


#usual response handles:
#	0x0003	INFO	00002a00-0000-1000-8000-00805f9b34fb
#	0x0023	BATTERY	00002a19-0000-1000-8000-00805f9b34fb
#	0x0027 	INCOMING_CALL_DATA	f000ff11-0451-4000-b000-000000000000 : paractically any bitmap to be displayed on the screen
#	0x002c	ALARM	f000ff01-0451-4000-b000-000000000000
#	0x002f	USER	f000ff02-0451-4000-b000-000000000000
#	0x0032	SPORT	f000ff03-0451-4000-b000-000000000000
#	0x0035	LED	f000ff04-0451-4000-b000-000000000000
#	0x0037	DATE	f000ff05-0451-4000-b000-000000000000
#	0x003a	PAIR	f000ff06-0451-4000-b000-000000000000
#	0x003c	DAILY	f000ff07-0451-4000-b000-000000000000
#	0x003f	SEDENTARY	f000ff08-0451-4000-b000-000000000000
#	0x0041	POWER_SAVING	f000ff09-0451-4000-b000-000000000000



###################################### WRITING DATE + TIME TO DEVICE ###################################
setDateTime ()
{

RES=`gatttool -i $HCI -t random -b $BDEV --char-read --handle=0x0037`

#bytes as integers:
declare -i IYY IMM IDD IHH IMT ISS 

ISS=`date +%S`
IYY=`date +%y`
IMM=`date +%m`-1
IDD=`date +%d`-1
IHH=`date +%H`
IMT=`date +%M`

#echo $IYY $IMM $IDD $IHH $IMT $ISS

YY=`printf "%02x" $IYY`
MM=`printf "%02x" $IMM`
DD=`printf "%02x" $IDD`
HH=`printf "%02x" $IHH`
MT=`printf "%02x" $IMT`
SS=`printf "%02x" $ISS`

DTSTRING=$YY$MM$DD$HH$MT$SS
RES=${RES// /}
RES=${RES//Characteristicvalue\/descriptor:/}

# in case if the difference is not greater than 15 seconds: do nothing
# 16 or more seconds: update the time on the device
if [[ "${RES:0:11}" == "${DTSTRING:0:11}" ]]; then 

	echo "DateTime:	Device and computer have the same date time. Skipping."
	echo "$RES ~=~ $DTSTRING"

else
	echo "Setting datetime on the device."
	echo "${RES:0:11} => ${DTSTRING:0:11}"
	gatttool -i $HCI -t random -b $BDEV --char-write-req --handle=0x0037 --value=$DTSTRING
fi
}

#END ###################################### WRITING DATE + TIME TO DEVICE ###################################

####################################### SET USER HEIGHT, WEIGHT, etc ###################################
setUserData ()
{
#read current user settings:
gatttool -i $HCI -t random -b $BDEV --char-read --handle=0x002f

# [height] [weight] [m/f] [age] [target_steps: LOBYTE HIBYTE]
#  aa 41 01 14 10 27


HEIGHT=`printf "%02x" $HEIGHT`
WEIGHT=`printf "%02x" $WEIGHT`
MF=`printf "%02x" $MF`
AGE=`printf "%02x" $AGE`
STEPSGOAL=`printf "%02x" $STEPSGOAL`

STEPSLO=${STEPSGOAL:2:2}
STEPSHI=${STEPSGOAL:0:2}

echo "NEW user values to be written  : $HEIGHT $WEIGHT $MF $AGE $STEPSLO $STEPSHI"
gatttool -i $HCI -t random -b $BDEV --char-write-req --handle=0x002f --value=$HEIGHT$WEIGHT$MF$AGE$STEPSLO$STEPSHI
}
#END ####################################### SET USER HEIGHT, WEIGHT, etc ###################################



#Read the battery state:
readBattery ()
{
	BATT=`gatttool -i $HCI -t random -b $BDEV --char-read --handle=0x0023`
	BATT=${BATT:33:2}
	BATT=$((16#$BATT))

	echo -e "Battery state:\t$BATT%"
	if [ -n $DBNAME ]; then
		IYY=`date +%y`
		IMM=`date +%m`
		IDD=`date +%d`
		IHH=`date +%H`
		IMT=`date +%M`
		if [ "$BATT" -gt 0 ]; then
			mysql -h${DBHOST} -u${DBUSER} -p${DBPASS} -e "insert into battery (date,time,battery,flag) values ('$IYY-$IMM-$IDD','$IHH:$IMT','$BATT',(select max(flag) from battery as b));" $DBNAME
		fi
	fi

}



#read daily data
readStepsToday ()
{
#				     d m y   steps-lobyte steps-hibyte
#aracteristic value/descriptor: a6 ff ff ff e9 17 00 00 89 ce 00 00 ed 0d 00 00
#prev day value/descriptor    : d2 12 00 0f ba 1c 00 00 ec f9 00 00 6d 11 00 00


	RES=`gatttool -i $HCI -t random -b $BDEV --char-read --handle=0x003c`
	#echo $RES

	#crop the needed data fields
	BCC=${RES:33:2}
	DD=${RES:36:2}
	MM=${RES:39:2}
	YY=${RES:42:2}
	if [ "$YY" = "ff" ]; then
		LOCALDAY=1
	fi

	#convert hexadecimal to decimal
	BCC=$((16#$BCC))
	DD=$((16#$DD))
	MM=$((16#$MM))
	YY=$((16#$YY))

	DLO=${RES:45:2}
	DHI=${RES:48:2}
	STEPS=$DHI$DLO

	#echo $STEPS
	echo -e "Steps:  \t$((16#$STEPS)) steps"

	DLO=${RES:57:2}
	DHI=${RES:60:2}
	DIST=$DHI$DLO
	DIST=$((16#$DIST))
	DIST=`echo 0.0001 \* $DIST | bc`
	echo -e "Distance: \t$DIST km"
	

	DLO=${RES:69:2}
	DHI=${RES:72:2}
	CAL=$DHI$DLO
	CAL=$((16#$CAL))
	CAL=`echo 0.1 \* $CAL | bc`
	echo -e "Calories: \t$CAL kcal"
	echo -e "(extra calories burned today besides your BMR)\n"

}

readMysql ()
{


RESULT=`mysql -h${DBHOST} -u${DBUSER} -p${DBPASS} -e "select x from table where id=$JID;" $DBNAME | tail -n +2`


}

saveDailyDataToCSV ()
{

#				     d m y   steps-lobyte steps-hibyte
#aracteristic value/descriptor: a6 ff ff ff e9 17 00 00 89 ce 00 00 ed 0d 00 00
#prev day value/descriptor    : d2 12 00 0f ba 1c 00 00 ec f9 00 00 6d 11 00 00
echo "daily totals: -----------------------------------"
HEADER="BCC\tYY-MM-DD\tSTEPS\tDST10m\tCAL*10"
echo -e $HEADER

if [ ! -e  dailyData.csv ]; then
	echo >> dailyData.csv -e $HEADER
fi

YY=1
MM=1
DD=1
while [ "$YY" -lt 255 ] && [ "$YY" -ne 0 ] && [ "$MM" -lt 255 ] && [ "$DD" -lt 255 ]; do 

	RES=`gatttool -i $HCI -t random -b $BDEV --char-read --handle=0x003c`
	#echo $RES
	#crop the needed data fields
	BCC=${RES:33:2}
	DD=${RES:36:2}
	MM=${RES:39:2}
	YY=${RES:42:2}
	if [ "$YY" = "ff" ]; then
		LOCALDAY=1
	fi

	#convert hexadecimal to decimal
	BCC=$((16#$BCC))
	DD=$((16#$DD))
	MM=$((16#$MM))
	YY=$((16#$YY))
	MM=`echo $MM + 1 | bc`
	DD=`echo $DD + 1 | bc`
	MM=`printf "%02d" $MM`
	DD=`printf "%02d" $DD`


	DLO=${RES:45:2}
	DHI=${RES:48:2}
	STEPS=$DHI$DLO
	STEPS=$((16#$STEPS))

	#echo $STEPS

	DLO=${RES:57:2}
	DHI=${RES:60:2}
	DIST=$DHI$DLO
	DIST=$((16#$DIST))
	#DIST=`echo 0.0001 \* $DIST | bc`
	#echo Distance: $DIST km
	

	DLO=${RES:69:2}
	DHI=${RES:72:2}
	CAL=$DHI$DLO
	CAL=$((16#$CAL))
	#CAL=`echo 0.1 \* $CAL | bc`
	#echo "Calories: $CAL kcal  (extra calories burned today besides your BMR)"
	if [ "$YY" -ne 0 ]; then 	
		if [ "$YY" -ne 255 ]; then 
			echo >> dailyData.csv -e "$BCC\t$YY-$MM-$DD\t$STEPS\t$DIST\t$CAL"
			if [ -n $DBNAME ]; then
				mysql -h${DBHOST} -u${DBUSER} -p${DBPASS} -e "insert into daily (bcc,date,steps,distance,cal) values ('$BCC','$YY-$MM-$DD','$STEPS','$DIST','$CAL');" $DBNAME
			fi
		else
			MM=''
			DD='\b\b\b\b\bToday    '
		fi
		echo -e "$BCC\t$YY-$MM-$DD\t$STEPS\t$DIST\t$CAL"
	fi

done

}


saveSportDataToCSV ()
{

#			       bcc min h  d  m  y  flag steps dist cal
#aracteristic value/descriptor: 22 1e 10 09 0b 0e 00 04 00 27 00 03 00 
#aracteristic value/descriptor: 23 23 10 09 0b 0e 00 22 00 2c 01 13 00 
#				0  1   2  3  4  5  6  7  8  9 10 11 12

echo -e "\ndetails: ----------------------------------------"

HEADER="BCC\tYY-MM-DD HH:MM\tSTEPS\tDST10m\tCAL*10\tFLAG"
echo -e $HEADER

if [ ! -e  sportData.csv ]; then
	echo >> sportData.csv -e $HEADER
fi

YY=1
MM=1
DD=1
while [ "$YY" -lt 255 ] && [ "$YY" -ne 0 ] && [ "$MM" -lt 255 ] && [ "$DD" -lt 255 ]; do 

	RES=`gatttool -i $HCI -t random -b $BDEV --char-read --handle=0x0032`
	#echo $RES
	#crop the needed data fields
	BCC=${RES:33:2}
	MIN=${RES:36:2}
	HH=${RES:39:2}
	DD=${RES:42:2}
	MM=${RES:45:2}
	YY=${RES:48:2}
	FLAG=${RES:51:2}
	DLO=${RES:54:2}
	DHI=${RES:57:2}
	STEPS=$DHI$DLO
	DLO=${RES:60:2}
	DHI=${RES:63:2}
	DIST=$DHI$DLO
	DLO=${RES:66:2}
	DHI=${RES:69:2}
	CAL=$DHI$DLO

	if [ "$YY" = "ff" ]; then
		LOCALDAY=1
	fi

	#convert hexadecimal to decimal
	BCC=$((16#$BCC))
	MIN=$((16#$MIN))
	HH=$((16#$HH))
	DD=$((16#$DD))
	MM=$((16#$MM))
	YY=$((16#$YY))
	FLAG=$((16#$FLAG))
	STEPS=$((16#$STEPS))
	DIST=$((16#$DIST))
	CAL=$((16#$CAL))

	MM=`echo $MM + 1 | bc`
	DD=`echo $DD + 1 | bc`
	MM=`printf "%02d" $MM`
	DD=`printf "%02d" $DD`
	MIN=`printf "%02d" $MIN`
	HH=`printf "%02d" $HH`


	if [ "$YY" -ne 0 ]; then 	
		if [ "$YY" -lt 255 ]; then 
			echo >> sportData.csv -e "$BCC\t$YY-$MM-$DD $HH:$MIN\t$STEPS\t$DIST\t$CAL\t$FLAG"
			if [ -n $DBNAME ]; then
				mysql -h${DBHOST} -u${DBUSER} -p${DBPASS} -e "insert into sport (bcc,date,time,steps,distance,cal,flag) values ('$BCC','$YY-$MM-$DD','$HH:$MIN','$STEPS','$DIST','$CAL','$FLAG') ON DUPLICATE KEY UPDATE bcc='$BCC', steps='$STEPS',distance='$DIST',cal='$CAL',flag='$FLAG';" $DBNAME
			fi
		else
			MM=''
			DD='\b\b\b\b\bNow:    '
			HH=`date +%H`
			MIN=`date +%M`
		fi
		echo -e "$BCC\t$YY-$MM-$DD $HH:$MIN\t$STEPS\t$DIST\t$CAL\t$FLAG"
	fi

done

}

####################################### SET A FEW ALARMS EVERY WEEK THE SAME TIME AND WEEKDAY ###################################
setClockAlarm ()
{
#read current user settings:

WEEK=0
HH=0
MM=1

#sleep - wake times
B0=`printf "%02x" $WEEK`
B4=`printf "%02x" $WEEK`
B1=`printf "%02x" $MM`
B2=`printf "%02x" $HH`

#alarm1

B3=01
HH=7
MM=0
B5=`printf "%02x" $MM`
B6=`printf "%02x" $HH`
B7=02

HH=8
MM=0
WEEK=255

B8=`printf "%02x" $WEEK`
B9=`printf "%02x" $MM`
B10=`printf "%02x" $HH`
B11=00
B12_31=0000000000000000000000000000000000000000
VALSTRING=$B0$B1$B2$B3$B4$B5$B6$B7$B8$B9$B10$B11$B12_31

#test/erase alarms
#VALSTRING=0000000100000002000000000000000000000000000000000000000000000000

echo -e "\n-------------------------------------------------"
echo "Writing ALARMS:"
#echo "NEW user values to be written  : $VALSTRING"
#gatttool -i $HCI -t random -b $BDEV --char-write-req --handle=0x002c --value=$VALSTRING

ALARMS=
while read -ra line; do
    ALARMS=$(echo "$ALARMS${line[0]}")
done < <(mysql -h${DBHOST} -u${DBUSER} -p${DBPASS} -e "select concat(lpad(hex(wdays),2,'0'), lpad(hex(mm),2,'0'), lpad(hex(hh),2,'0'),'00') as '0000000100000002' from alarm where bdev is not null and sync=0 order by n asc limit 3"  $DBNAME)




B20_31=000000000000000000000000

if [[ "$ALARMS$B20_31" == "000000000000000000000000" ]]; then 

	echo "Alarms:		Nothing to sync from the database. Skipping."
else

	RES=`gatttool -i $HCI -t random -b $BDEV --char-read --handle=0x002c`
	# replace all blanks
	RES=${RES// /}
	echo $RES

	echo "NEW user values to be written: $ALARMS$B20_31"
	gatttool -i $HCI -t random -b $BDEV --char-write-req --handle=0x002c --value=$ALARMS$B20_31

	rc=$?
	if [ "$rc" -eq 0 ]; then 
		mysql -h${DBHOST} -u${DBUSER} -p${DBPASS} -e "update alarm set sync=1 where bdev is not null and sync=0"  $DBNAME
	else
		echo "Writing alarms did not go through bluetooth connection. Updating at the next run."
	fi
fi

}
#END ####################################### SET USER HEIGHT, WEIGHT, etc ###################################

setUserSettings ()
{

#read current user settings:
#gatttool -i $HCI -t random -b $BDEV --char-read --handle=0x002f

# [height] [weight] [m/f] [age] [target_steps: LOBYTE HIBYTE]
#  aa 41 01 14 10 27

DATA=
while read -ra line; do
    DATA=$(echo "$DATA${line[0]}")
done < <(mysql -h${DBHOST} -u${DBUSER} -p${DBPASS} -e "select concat(lpad(hex(height),2,'0'), 
lpad(hex(weight),2,'0'), 
lpad(hex(gender),2,'0'),
lpad(hex(age),2,'0'),
right(lpad(hex(stepsgoal),4,'0'),2),
left(lpad(hex(stepsgoal),4,'0'),2)) as ''
from user where bdev is not null and sync=0 limit 1"  $DBNAME)



if [[ "x$DATA" == "x" ]]; then 

	echo "User Settings:	Nothing to sync from the database. Skipping."
else

	RES=`gatttool -i $HCI -t random -b $BDEV --char-read --handle=0x002f`
	# replace all blanks
	RES=${RES// /}
	echo $RES

	echo "NEW user values to be written: $DATA"
	gatttool -i $HCI -t random -b $BDEV --char-write-req --handle=0x002c --value=$DATA

	rc=$?
	if [ "$rc" -eq 0 ]; then 
		mysql -h${DBHOST} -u${DBUSER} -p${DBPASS} -e "update user set sync=1 where bdev is not null and sync=0"  $DBNAME
	else
		echo "Writing user settings did not go through bluetooth connection. Syncing at the next run."
	fi
fi

}


#Running some functions defined above:
Init

#gatttool -i $HCI -t random -b $BDEV --char-read --handle=0x002c
#gatttool -i $HCI -t random -b $BDEV --char-read --handle=0x0027
#exit

#setUserData
setDateTime

readBattery
#readStepsToday
#saveDailyDataToCSV
saveSportDataToCSV
setClockAlarm
setUserSettings


#gatttool -i $HCI -t random -b $BDEV --char-write-req --handle=0x0027 --value=02020202020ff00ff00ff00ff00ff00ff00ff00ff00ff00ff00


echo -e "\n-------------------------------------------------"
#echo "Available also: setUserData, setDateTime"
#echo -e "-------------------------------------------------"
