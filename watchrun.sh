#!/bin/bash

#just a little script for testing purposes
#it runs the main synchronisation script every 12 minutes

while true
do
	# loop infinitely
	timeout 12m /home/a/fitbit/bracelet_i5_private.sh
	echo "going to sleep for 12 minutes"
	sleep 12m
done
