<?php
include('phpgraphlib.php');
include('dbconnection.php');
include('settings.php');


$date='';
if(isset($_GET['date']))
	{ $date=$_GET['date']; }


#print_r($_GET); 
$DEBUG=false;



mysql_query("insert ignore into activity (date) select date from sport group by date");


mysql_query("update activity set act1=0, act2=0, act3=0");
mysql_query("update activity a, (select date, count(date)*5 as minutes from sport where cal > $s_act1 group by date) s set a.act1=s.minutes where a.date=s.date");
mysql_query("update activity a, (select date, count(date)*5 as minutes from sport where cal > $s_act2 group by date) s set a.act2=s.minutes where a.date=s.date");
mysql_query("update activity a, (select date, count(date)*5 as minutes from sport where cal > $s_act3 group by date) s set a.act3=s.minutes where a.date=s.date");


$sql="select date, ifnull(act1,0) act1, ifnull(act2,0) act2, ifnull(act3,0) act3 from activity order by date asc";

$result = mysql_query($sql) or die('Query failed: ' . mysql_error());
$graph = new PHPGraphLib(max(400,30*mysql_num_rows($result)), 350);

$data1=array();
$data2=array();
$data3=array();

if ($result) {
  while ($row = mysql_fetch_assoc($result)) {
      $date=$row["date"];
      $date="20$date";
      $wday=date('D', strtotime($date));
      $date="$date $wday"; 
      
      $data1[$date]=$row["act1"];
      $data2[$date]=$row["act2"];
      $data3[$date]=$row["act3"];
  }
}



if($DEBUG){
	print_r($data1);
	print_r($data2);
	print_r($data3);
	print(mysql_num_rows($result));
	exit;
}

//configure graph
/**/
$graph->addData($data3, $data2, $data1);
$graph->setTitle("Active minutes ");
$graph->setBarColor('red', 'yellow', 'green');
$graph->setupYAxis(12, 'black');
$graph->setupXAxis(20);
$graph->setGrid(true);
$graph->setLegend(true);
$graph->setTitleLocation('left');
$graph->setTitleColor('blue');
$graph->setLegendOutlineColor('white');
$graph->setLegendTitle('intense', 'moderate', 'active');
$graph->setXValuesHorizontal(false);
$graph->setDataValues(true);
$graph->setDataValueColor('navy');
$graph->setGoalLine(30);
$graph->setGoalLineColor('red');
$graph->createGraph();
/**/
?>



