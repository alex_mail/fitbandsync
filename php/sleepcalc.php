<PRE><BIG>
<?php
include('dbconnection.php');

// count the sleep hours.
// for now it's done using a simple exponential average over the time  and cutting above threshold

# General directives for calculating sleep:
# 1. it has to be at least 1 hour long
#	any rest shorter than 1 hour is not counted as sleep
# 2. if there are 2 or more blocks of rest with a little activity between them, 
#	ie graph looks like this: _____I____II___I__ then it's still called sleep
#	the acitivity between them is called 'restlessness' and it can't be too intense
# 3. the calculation of sleep will start with the first rest block AFTER the last sleep
# 4. it will run until the datetime = NOW()
# 5. need a function to fill missing times in the db up until last sync or now()



$time_start = microtime(true); 
#$sql= "delete from sport where bcc<0 and flag<0;";
#mysql_query($sql) or die('Query failed: ' . mysql_error());



$sql= "DROP TEMPORARY TABLE IF EXISTS tmpsp;";
mysql_query($sql) or die('Query failed: ' . mysql_error());


$sql="CREATE TEMPORARY TABLE IF NOT EXISTS tmpsp AS (
SELECT  date, hhmm as time from sport s
join hhmm where s.date>'15-02-05'
group by date, hhmm )";
mysql_query($sql) or die('Query failed: ' . mysql_error());

$sql="insert ignore into sport (bcc,date,time,steps,distance,cal,flag)
select -1,date,time,0,0,0,-1 from tmpsp 
where concat (date,time) not in (select concat(date, time) from sport where date>'15-02-05') 
and  concat ('20',date,' ',time) < NOW()";
mysql_query($sql) or die('Query failed: ' . mysql_error());

$sql= "DROP TEMPORARY TABLE IF EXISTS tmpsp;";
mysql_query($sql) or die('Query failed: ' . mysql_error());
  
#avg steps start



$sql= "select * from sport order by date asc, time asc;";
$result = mysql_query($sql) or die('Query failed: ' . mysql_error());

$expavg=50.0;
$comma='';

$sql="UPDATE sport set flag=-20 where sport_id IN ( "; 
if ($result) {
  while ($row = mysql_fetch_assoc($result)) {
      $id=$row["sport_id"];
      $val=$row["steps"];
      $expavg = 0.5*$expavg + 0.5*$val;
      if($expavg<1) {
	$sql = $sql.$comma.$id;
	if($comma==''){$comma=',';};
      }
      //mysql_query("UPDATE sport set flag=$expavg where sport_id=$id") or die('Query failed: ' . mysql_error());


   }
}


$sql = $sql.')';
if(!$comma=='') {
	mysql_query($sql) or die('Query failed: ' . mysql_error());
}

#echo '<br>'.$sql.' '.(microtime(true) - $time_start) if $DEBUG;
#$time_start = microtime(true); 


echo "<hr>";

$sql="delete from activity"; 
$result = mysql_query($sql) or die('Query failed: ' . mysql_error());

$sql="insert into activity(date,sleep) select date, count(date)*5/60 as sleep from sport 
where flag = -20
group by date
order by
	date,
	time ;";

$result = mysql_query($sql) or die('Query failed: ' . mysql_error());

$sql="select date, sleep from activity order by date asc"; 

$result = mysql_query($sql) or die('Query failed: ' . mysql_error());


if ($result) {
  while ($row = mysql_fetch_assoc($result)) {
      $date=$row["date"];
      $sleep=number_format($row["sleep"],2);
      $date="20$date";
      $wday=date('D', strtotime($date));
      echo "$wday $date:&nbsp;&nbsp;$sleep<hr>";
 
   }
}

//$sql= "delete from sport where bcc<0 and flag<0;";
//mysql_query($sql) or die('Query failed: ' . mysql_error());

?>
<br><img src="sleepgraph.php">


