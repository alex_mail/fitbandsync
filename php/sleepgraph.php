<?php
include('phpgraphlib.php');
include('dbconnection.php');
$DEBUG=false;


$date='';
if(isset($_GET['date']))
	{ $date=$_GET['date']; }


#print_r($_GET); 


//get data from database
$sql="select date, sleep, (select avg(sleep) from activity) as avgsleep from activity order by date asc";
#echo $sql;

$result = mysql_query($sql) or die('Query failed: ' . mysql_error());
  
$data1=array();
$avgsleep=0;
if ($result) {
  while ($row = mysql_fetch_assoc($result)) {
      $date=$row["date"];
      $avgsleep=$row["avgsleep"];
      $date="20$date";
      $wday=date('D', strtotime($date));
      $date="$date $wday"; 
      
      //add to data areray
      $data1[$date]=$row["sleep"];
  }
}

if($DEBUG){
	print_r($data1);
	print(mysql_num_rows($result));
	exit;
}

//configure graph
$graph = new PHPGraphLib(max(400,30*mysql_num_rows($result)), 350);
$graph->addData($data1);
$graph->setTitle("Sleep time hours");
$graph->setBarColor('yellow', 'red');
$graph->setupYAxis(12, 'black');
$graph->setupXAxis(20);
$graph->setGrid(true);
//$graph->setLegend(true);
$graph->setTitleLocation('left');
$graph->setTitleColor('blue');
//$graph->setLegendOutlineColor('white');
//$graph->setLegendTitle('Hours of sleep');
$graph->setXValuesHorizontal(false);
$graph->setDataValues(true);
$graph->setDataValueColor('navy');
$graph->setGoalLine($avgsleep);
$graph->setGoalLineColor('red');
$graph->createGraph();

?>



