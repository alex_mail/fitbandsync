<?php
include('phpgraphlib.php');
include('dbconnection.php');
$DEBUG=false;


$date='';
if(isset($_GET['date']))
	{ $date=$_GET['date']; }


#print_r($_GET); 


//get data from database
$sql="select date, sum(steps) as steps, sum(distance) as distance, sum(cal) as cal from sport group by date order by date asc";
#echo $sql;

$result = mysql_query($sql) or die('Query failed: ' . mysql_error());
  
$data1=array();
$data2=array();

$steps=0;
$cal=0;
$distance=0;

if ($result) {
  while ($row = mysql_fetch_assoc($result)) {
      $date=$row["date"];
      $date="20$date";
      $wday=date('D', strtotime($date));
      $date="$date $wday"; 
      
      //add to data areray
      $data1[$date]=$row["steps"];
      $data2[$date]=$row["cal"];;

      $steps=$row["steps"];
      $cal=$row["cal"]/10;
      $distance=number_format($row["distance"]/10000.0,2);
  }
}

if($DEBUG){
	print_r($data1);
	print_r($data2);
	print(mysql_num_rows($result));
	exit;
}

//configure graph
$graph = new PHPGraphLib(max(400,30*mysql_num_rows($result)), 350);
$graph->addData($data2, $data1);
$graph->setTitle("Today: $steps steps $cal kcal $distance km");
$graph->setBarColor('yellow', 'red');
$graph->setupYAxis(12, 'black');
$graph->setupXAxis(20);
$graph->setGrid(true);
$graph->setLegend(true);
$graph->setTitleLocation('left');
$graph->setTitleColor('blue');
$graph->setLegendOutlineColor('white');
$graph->setLegendTitle('Cal', 'Steps');
$graph->setXValuesHorizontal(false);
$graph->setDataValues(false);
$graph->createGraph();

?>



