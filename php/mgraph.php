<?php
include('phpgraphlib.php');
include('dbconnection.php');
include('settings.php');

$date=$_GET['date'];
#print_r($_GET);

$title="Today";

if($date=='') { 
		$date='(select max(date) from sport)';
	}else { 
		$title="20$date";
		$date="'$date'"; 
	}


//get data from database
$sql="select bcc, time, steps, cal, flag from vsport where date=$date or date is null group by time order by time asc, steps desc";
#echo $sql;

$result = mysql_query($sql) or die('Query failed: ' . mysql_error());
  
$data1=array();
$data2=array();


if ($result) {
  while ($row = mysql_fetch_assoc($result)) {
      $time=$row["time"];
      
      //add to data areray
      $data1[$time]=$row["steps"];
      $data2[$time]=$row["cal"];

  }
}

  
//configure graph
$graph = new PHPGraphLib(20*mysql_num_rows($result), 350);
$graph->addData($data2, $data1);
$graph->setTitle("Steps");
$graph->setBarColor('green', 'blue');
$graph->setTitle($title);
$graph->setupYAxis(12, 'black');
$graph->setupXAxis(20);
$graph->setGrid(true);
$graph->setLegend(true);
$graph->setTitleLocation('left');
$graph->setTitleColor('blue');
$graph->setLegendOutlineColor('white');
$graph->setLegendTitle('Cal', 'Steps');
$graph->setXValuesHorizontal(false);
$graph->setDataValues(false);
$graph->setGoalLine($s_act3,'red');
$graph->setGoalLine($s_act2,'yellow');
$graph->setGoalLine($s_act1,'green');
$graph->createGraph();
?>

