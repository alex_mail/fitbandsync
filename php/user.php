<html>
<head>
<?php

$nuser=3; 
$save=$_GET['save'];
if($save=="1"){
	echo "<meta http-equiv='refresh' content='5; url=user.php'>";
}

?>

<title="i5 fitness bracelet user settings">
<SCRIPT TYPE="text/JavaScript">
    function validateThis(inputField) {
        var isValid = IsNumeric(inputField.value);

        if (isValid) {
            inputField.style.backgroundColor = '#bfa';
        } else {
            inputField.style.backgroundColor = '#fba';
        }

        return isValid;
    }
</SCRIPT>
</head>
<body>

<style>
.datagrid table { border-collapse: collapse; text-align: left; width: 100%; } .datagrid {font: normal 12px/150% Arial, Helvetica, sans-serif; background: #fff; overflow: hidden; border: 1px solid #006699; -webkit-border-radius: 3px; -moz-border-radius: 3px; border-radius: 3px; }.datagrid table td, .datagrid table th { padding: 3px 10px; }.datagrid table thead th {background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #006699), color-stop(1, #00557F) );background:-moz-linear-gradient( center top, #006699 5%, #00557F 50% );filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#006699', endColorstr='#00557F');background-color:#006699; color:#FFFFFF; font-size: 15px; font-weight: bold; border-left: 1px solid #0070A8; } .datagrid table thead th:first-child { border: none; }.datagrid table tbody td { color: #00557F; border-left: 1px solid #E1EEF4;font-size: 12px;font-weight: normal; }.datagrid table tbody .alt td { background: #E1EEf4; color: #00557F; }.datagrid table tbody td:first-child { border-left: none; }.datagrid table tbody tr:last-child td { border-bottom: none; }.datagrid table tfoot td div { border-top: 1px solid #006699;background: #E1EEf4;} .datagrid table tfoot td { padding: 0; font-size: 12px } .datagrid table tfoot td div{ padding: 2px; }.datagrid ax { margin: 0; padding:0; list-style: none; text-align: right; }.datagrid button { display: inline; }.datagrid button, datagrid a { text-decoration: none; display: inline-block;  padding: 2px 8px; margin: 1px;color: #FFFFFF;border: 1px solid #006699;-webkit-border-radius: 3px; -moz-border-radius: 3px; border-radius: 3px; background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #006699), color-stop(1, #00557F) );background:-moz-linear-gradient( center top, #006699 5%, #00557F 100% );filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#006699', endColorstr='#00557F');background-color:#006699; }.datagrid button, .datagrid a { text-decoration: none;border-color: #00557F; color: #FFFFFF; background: none; background-color:#006699;}div.dhtmlx_window_active, div.dhx_modal_cover_dv { position: fixed !important; }
.smallbutton {
display: block;
width: 50px;
height: 18px;
background: #006699;
padding: 4px;
text-align: center;
border-radius: 4px;
color: white;
font-weight: normal;
}
</style>


<?php
include('dbconnection.php');
	
	if($save=="1")
	{
		echo "<span class=datagrid><h3>Settings saved</h3>";
		echo "The new values will be written to the bracelet at the next synchronization<p></span>";
		
		$bdev=$_GET['bdev'];
		$height=$_GET['height'];
		$weight=$_GET['weight'];
		$age=$_GET['age'];
		$gender=$_GET['gender'];
		$stepsgoal=$_GET['stepsgoal'];


		$sql="insert into user (bdev,height,weight,gender,age,stepsgoal,sync) values
		('$bdev',$height,$weight,$gender,$age,$stepsgoal,0) 
		ON DUPLICATE KEY UPDATE height=$height, weight=$weight, age=$age, stepsgoal=$stepsgoal, gender=$gender, sync=0";
		#echo $sql."<br>\n";
		$result = mysql_query($sql) or die('Query failed: ' . mysql_error());
	
		echo "<a class=smallbutton href='user.php'>Ok</a>";
		exit;
	}



$arr=array();
//get data from database
$sql="select bdev,height,weight,gender,age,stepsgoal,sync from user limit 1";
#echo $sql;
$result = mysql_query($sql) or die('Query failed: ' . mysql_error());
if(mysql_num_rows($result)==1)
{
	$arr = mysql_fetch_assoc($result);
}else{
	$arr["height"]="170";
	$arr["weight"]="65";
	$arr["gender"]="1";
	$arr["age"]="35";
	$arr["stepsgoal"]="10000";
}


$height=$arr["height"];
$weight=$arr["weight"];
$gender=$arr["gender"];
$age=$arr["age"];

?>


<p>
<table width=70% border=0>
<th><td>
<form name=form1 action=user.php method="GET">
<div class="datagrid">
<table width="30%">
<thead>
<tr>
<th align=right></th>
<th align=left>&nbsp;</th>
</tr>
</thead>
<tfoot><tr><td colspan="2"><div>
<button name=save action=submit value=1>Save values</button></div></td></tr></tfoot>

<tbody>
<?php

#onchange=\"validateThis(this);\"


	echo "\n<tr class=alt>";
	$pname="bdev";
	echo "<td align=right>Device</td>";
	echo "<td align=left><input type=text id=$pname name=$pname size=10  maxlength=17  STYLE=\"background-color: #fafafa;\" value='$arr[$pname]'></td>";
	echo "\n</tr>"; 
	echo "\n<tr class=alt><td colspan=2>&nbsp</td></tr>";


	echo "\n<tr>";
	$pname="height";
	echo "<td align=right>Height</td>";
	echo "<td align=left><input type=number min=1 max=300 step=1 id=$pname name=$pname size=2  maxlength=3  STYLE=\"background-color: #fafafa;\" value='$arr[$pname]'> cm</td>";
	echo "\n</tr>"; 

	echo "\n<tr class=alt>";
	$pname="weight";
	echo "<td align=right>Weight</td>";
	echo "<td align=left><input type=number min=1 max=300 step=1 id=$pname name=$pname size=2  maxlength=3  STYLE=\"background-color: #fafafa;\" value='$arr[$pname]'> kg</td>";
	echo "\n</tr>"; 


	echo "\n<tr>";
	$pname="gender";
	$male="";
	$female="";
	
	if($arr[$pname]=="0")
		{$male="selected";}
	else
		{$female="selected";}

	echo "<td align=right>Gender</td>";
	echo "<td align=left> <select name='$pname'>
  <option value='0' $male>Male</option>
  <option value='1' $female>Female</option>
 </select> </td>";
	echo "\n</tr>"; 

	echo "\n<tr class=alt>";
	$pname="age";
	echo "<td align=right>Age</td>";
	echo "<td align=left><input type=number min=0 max=200 step=1 id=$pname name=$pname size=3  maxlength=3  STYLE=\"background-color: #fafafa;\" value='$arr[$pname]'></td>";
	echo "\n</tr>"; 

	echo "\n<tr>";
	$pname="stepsgoal";
	echo "<td align=right>Daily steps goal</td>";
	echo "<td align=left><input type=number min=1 max=99999 step=1 id=$pname name=$pname size=5  maxlength=5  STYLE=\"background-color: #fafafa;\" value='$arr[$pname]'> steps</td>";
	echo "\n</tr>"; 


?>
</tbody>
</table>
</td></td></table>
</div>
<p>
<span class=datagrid><a class=smalbutton href='main.php'> &nbsp;&nbsp;BACK&nbsp;&nbsp; </a></span>

</form>

<?php 
#print_r($atimes); 
#print_r($onoffs);

#BMR Formula: We use standard MD Mifflin-St Jeor equation:
#

$malbmr=9.99 * $weight + 6.25*$height - 4.92*$age + 5;
$fembmr=9.99 * $weight + 6.25*$height - 4.92*$age -161;

$maltee = 864 - 9.72 * $age + 1.0 * (14.2 * $weight + 5.03 * $height);
$femtee = 387 - 7.31 * $age + 1.0 * (10.9 * $weight + 6.607 * $height);

$bmi=10000.0*$weight/($height*$height);

$bmirange="Underweight";
if($bmi>18.5){$bmirange="Normal BMI range";};
if($bmi>25){$bmirange="Overweight";};
if($bmi>30){$bmirange="Obese";};


echo "<p>&nbsp;";
echo "<br> Your male BMR (Basic Metabolic Rate) is: ".number_format($malbmr,0)." kcal";
echo "<br> Your female BMR (Basic Metabolic Rate) is: ".number_format($fembmr,0)." kcal";
echo "<br> Your male TEE (Total Energy Expenditure) is: ".number_format($maltee,0)." kcal";
echo "<br> Your female TEE (Total Energy Expenditure) is: ".number_format($femtee,0)." kcal";
echo "<br> Your BMI (Body Mass Index) is: ".number_format($bmi,1);
echo "<br> You are: <b>".$bmirange."</b>";



?>
<br>
<img src="Body_mass_index_chart.png" width="65%">
<p>
<small>Source: <a href="https://en.wikipedia.org/wiki/Body_mass_index">Wikipedia</a></small>
