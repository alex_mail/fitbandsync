<?php
include('phpgraphlib.php');
include('dbconnection.php');


$date='';
if(isset($_GET['date']))
	{ $date=$_GET['date']; }


#print_r($_GET);


//get data from database

$sql="select concat(date,' ', time) as date, battery from battery where battery!='0' group by order by date asc, time asc";
$sql="select concat(date,' ', time) as date, avg(battery) as battery, flag from battery group by date, left(time,2)%6 order by date asc";

#echo $sql;

$result = mysql_query($sql) or die('Query failed: ' . mysql_error());
  
$data1=array();

if ($result) {
  while ($row = mysql_fetch_assoc($result)) {
      $time=$row["date"];
      $data1[$time]=number_format($row["battery"],0);
  }
}

if($DEBUG){
	print_r($data1);
	print(mysql_num_rows($result));
	exit;
}

//configure graph
$graph = new PHPGraphLib(max(400,30*mysql_num_rows($result)), 350);
$graph->addData($data1);
$graph->setTitle("Battery state");
$graph->setBarColor('green');
$graph->setupYAxis(12, 'black');
$graph->setupXAxis(20);
$graph->setGrid(true);
$graph->setLegend(true);
$graph->setTitleLocation('left');
$graph->setTitleColor('blue');
$graph->setLegendOutlineColor('white');
$graph->setLegendTitle('Cal', 'Steps');
$graph->setXValuesHorizontal(false);
$graph->setDataValues(true);
$graph->createGraph();
?>

