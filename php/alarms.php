<html>
<head>
<?php
$nalarms=3; //max number of alarms
$save=$_GET['save'];
if($save=="1"){
	echo "<meta http-equiv='refresh' content='5; url=alarms.php'>";
}
?>

<title="i5 fitness bracelet">
<SCRIPT TYPE="text/JavaScript">
    function validateHhMm(inputField, chkbname) {
        var isValid = /^([0-1]?[0-9]|2[0-4]):([0-5][0-9])(:[0-5][0-9])?$/.test(inputField.value);

        if (isValid) {
            inputField.style.backgroundColor = '#bfa';
	    document.getElementById(chkbname).checked = true;
        } else {
            inputField.style.backgroundColor = '#fba';
	    document.getElementById(chkbname).checked = false;
        }

	document.getElementById("infosaved").visible = false;

        if (isValid) {
	    document.getElementById(chkbname).checked = true;
        } else {
	    document.getElementById(chkbname).checked = false;
        }


        return isValid;
    }
</SCRIPT>
</head>
<body>

<style>
.datagrid table { border-collapse: collapse; text-align: left; width: 100%; } .datagrid {font: normal 12px/150% Arial, Helvetica, sans-serif; background: #fff; overflow: hidden; border: 1px solid #006699; -webkit-border-radius: 3px; -moz-border-radius: 3px; border-radius: 3px; }.datagrid table td, .datagrid table th { padding: 3px 10px; }.datagrid table thead th {background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #006699), color-stop(1, #00557F) );background:-moz-linear-gradient( center top, #006699 5%, #00557F 100% );filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#006699', endColorstr='#00557F');background-color:#006699; color:#FFFFFF; font-size: 15px; font-weight: bold; border-left: 1px solid #0070A8; } .datagrid table thead th:first-child { border: none; }.datagrid table tbody td { color: #00557F; border-left: 1px solid #E1EEF4;font-size: 12px;font-weight: normal; }.datagrid table tbody .alt td { background: #E1EEf4; color: #00557F; }.datagrid table tbody td:first-child { border-left: none; }.datagrid table tbody tr:last-child td { border-bottom: none; }.datagrid table tfoot td div { border-top: 1px solid #006699;background: #E1EEf4;} .datagrid table tfoot td { padding: 0; font-size: 12px } .datagrid table tfoot td div{ padding: 2px; }.datagrid ax { margin: 0; padding:0; list-style: none; text-align: right; }.datagrid button { display: inline; }.datagrid button, datagrid a { text-decoration: none; display: inline-block;  padding: 2px 8px; margin: 1px;color: #FFFFFF;border: 1px solid #006699;-webkit-border-radius: 3px; -moz-border-radius: 3px; border-radius: 3px; background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #006699), color-stop(1, #00557F) );background:-moz-linear-gradient( center top, #006699 5%, #00557F 100% );filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#006699', endColorstr='#00557F');background-color:#006699; }.datagrid button, .datagrid a { text-decoration: none;border-color: #00557F; color: #FFFFFF; background: none; background-color:#006699;}div.dhtmlx_window_active, div.dhx_modal_cover_dv { position: fixed !important; }
.smallbutton {
display: block;
width: 50px;
height: 18px;
background: #006699;
padding: 4px;
text-align: center;
border-radius: 4px;
color: white;
font-weight: normal;
}
</style>


<?php
include('dbconnection.php');
	
	if($save=="1")
	{
		echo "<span class=datagrid><h3>Alarms saved</h3>";
		echo "The new values will be written to the bracelet at the next synchronization<p></span>";
		$hhmm=array();

		for ($n = 1; $n <= $nalarms; $n++)
		{  
			$wdval=0;
			$atime=$_GET["alarm_$n"];
			for ($wd = 0; $wd < 7; $wd++)
			{
				$idx="a$n$wd";
				if($_GET["$idx"] <> "")
				{
					$wdval=$wdval|(1<<$wd);
				}

			}
			if($atime==""){$atime="07:00";}
			$hhmm=explode(":", $atime);

			$sql="insert into alarm (bdev,n,wdays,hh,mm,sync) values ('00:00:00:00:00:00',$n,$wdval,$hhmm[0],$hhmm[1],0) 
			ON DUPLICATE KEY UPDATE wdays=$wdval, hh=$hhmm[0], mm=$hhmm[1], sync=0";
			#echo $sql."<br>\n";
			$result = mysql_query($sql) or die('Query failed: ' . mysql_error());
		}
	echo "<a class=smallbutton href='alarms.php'>Ok</a>";
	exit;
	}
?>


<p>
<table width=70% border=0>
<th><td>
<form name=form1 action=alarms.php method="GET">
<div class="datagrid">
<table width="50%">
<thead>
<tr>
<th align=center>Time</td>
<th align=center>Sun</td>
<th align=center>Mon</td>
<th align=center>Tue</td>
<th align=center>Wed</td>
<th align=center>Thu</td>
<th align=center>Fri</td>
<th align=center>Sat</td>
</tr>
</thead>
<tfoot><tr><td colspan="8"><div>
<button name=save action=submit value=1>Save values</button>
</div></td></tr></tfoot>

<tbody>
<?php

//get data from database
$sql="select n, wdays, concat(lpad(hh,2,'0'),':',lpad(mm,2,'0')) as time, sync from alarm  group by n, hh, mm order by n asc, hh asc, mm asc";
#echo $sql;
$result = mysql_query($sql) or die('Query failed: ' . mysql_error());

  

$atimes=array();
$onoffs=array();

if ($result) {
  while ($row = mysql_fetch_assoc($result)) {
      $n=$row["n"];
      $wdays=$row["wdays"];
      $time=$row["time"];
      for ($wd = 0; $wd < 7; $wd++)
      {
      	$idx="a$n$wd";
	$wdval=(1<<$wd);
	if(($wdays&$wdval)>0)
	{
		$onoffs[$idx]=1;
	}else{
		$onoffs[$idx]=0;

	}	
	$atimes["a$n"]=$time;
      }
  }
}


for ($n = 1; $n <= $nalarms; $n++)
{  
	$time=$atimes["a$n"];
	$alt="";
	if($n%2==0){$alt="class=alt";}
	echo "\n<tr $alt>";
	echo "<td align=center><input type=text id=alarm_$n name=alarm_$n size=3  maxlength=5 onchange=\"validateHhMm(this, 'on_$wd$n');\" STYLE=\"background-color: #fafafa;\" value='$time'></td>";
	for ($wd = 0; $wd < 7; $wd++) 
	{ 
		$wdval=(1<<$wd);
		$onoff;
		$idx="a$n$wd";
		if($onoffs[$idx]==1){
			$onoff="checked=checked";
		}else{
			$onoff="";
		}
		

		echo "\n<td align=center><input type=checkbox id='$idx' name='$idx' value='$wdval' $onoff></td>";
	}
	echo "\n</tr>"; 

}

?>
</tbody>
</table>
</div>
</td></td></table>
<p>
<span class=datagrid><a class=smalbutton href='main.php'> &nbsp;&nbsp;BACK&nbsp;&nbsp; </a></span>

<?php 
#print_r($atimes); 
#print_r($onoffs);
?>

</form>
